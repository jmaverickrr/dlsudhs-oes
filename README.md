## Description
It is an online enrollment system developed using PHP, MySQL, HTML5/CSS3 and JavaScript. It is capable of application of new students, reenrollment of previous students, assessment of fees, dummy online payment system, encoding of grades, automated generation of schedules, sections, teaching load, generation of reports and other basic features of enrollment systems.
Only the documentation/manual is available.

## Contributors
Juvar F. Abrera (http://juvarabrera.com/)
Jarrell Maverick R. Remulla